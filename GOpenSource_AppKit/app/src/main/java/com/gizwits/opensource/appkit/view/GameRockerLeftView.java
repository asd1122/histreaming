package com.gizwits.opensource.appkit.view;


import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.gizwits.opensource.appkit.ControlModule.GosCarControlActivity;
import com.gizwits.opensource.appkit.R;
import com.hisilicion.histreaming.GizWifiDevice;
import com.hisilicion.histreaming.GizWifiSDK;

public class GameRockerLeftView extends View {
    private Paint outerCirclePaint;
    private Paint middleCirclePaint;
    private Paint innerCirclePaint;

    /** 内圆中心x坐标 */
    private double innerCenterX;
    /** 内圆中心y坐标 */
    private double innerCenterY;
    /** view中心点x坐标 */
    private float viewCenterX;
    /** view中心点y左边 */
    private float viewCenterY;
    /** view宽高大小，设定宽高相等 */
    private int size;
    /** 外圆半径 */
    private int outerCircleRadius;
    /** 中间圆半径 **/
    private int middleCircleRadius;
    /** 内圆半径 */
    private int innerCircleRadius;
    /**  背景图 **/
    private GameRockerViewBg gameRockerViewBg;
    private int pictureChange;

    private GizWifiDevice device;

    private GosCarControlActivity gosCarControlActivity;

    private int lastCommand = 0;


    public GameRockerLeftView(Context context) {
        super(context);
        init();
    }

    public GameRockerLeftView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

//        device = (GizWifiDevice) getIntent().getParcelableExtra("GizWifiDevice");
        init();
    }

    private void init() {
        outerCirclePaint = new Paint();
        outerCirclePaint.setColor(getResources().getColor(R.color.alert_blue));
        outerCirclePaint.setAntiAlias(true);
        outerCirclePaint.setMaskFilter(new BlurMaskFilter(50, BlurMaskFilter.Blur.INNER));

        middleCirclePaint = new Paint();
        middleCirclePaint.setColor(getResources().getColor(R.color.tomato));
        middleCirclePaint.setStrokeWidth(5);
        middleCirclePaint.setStyle(Paint.Style.STROKE);
        middleCirclePaint.setAntiAlias(true);
        middleCirclePaint.setMaskFilter(new BlurMaskFilter(50, BlurMaskFilter.Blur.INNER));

        innerCirclePaint = new Paint();
        innerCirclePaint.setAlpha(130);
        innerCirclePaint.setColor(getResources().getColor(R.color.white));
        innerCirclePaint.setAntiAlias(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        size = getMeasuredWidth();
        System.out.println("size:"+size);
        setMeasuredDimension(size, size);

        innerCenterX = size/2;
        innerCenterY = size/2;
        viewCenterX = size/2;
        viewCenterY = size/2;
        outerCircleRadius = size/2;
        middleCircleRadius = size/3;
        innerCircleRadius = size/7;
        gameRockerViewBg = new GameRockerViewBg(BitmapFactory.decodeResource(getResources(), R.mipmap.uparrow),
                BitmapFactory.decodeResource(getResources(), R.mipmap.uparrowclick),
                BitmapFactory.decodeResource(getResources(), R.mipmap.downarrow),
                BitmapFactory.decodeResource(getResources(), R.mipmap.downarrowclick),
                new Rect(outerCircleRadius-30, 0, outerCircleRadius+30, 60),
                new Rect(outerCircleRadius-30, outerCircleRadius+middleCircleRadius+5, outerCircleRadius+30, outerCircleRadius+middleCircleRadius+65));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(viewCenterX, viewCenterY, outerCircleRadius, outerCirclePaint);
        canvas.drawCircle(viewCenterX, viewCenterY, middleCircleRadius, middleCirclePaint);
        canvas.drawCircle((float) innerCenterX, (float) innerCenterY, innerCircleRadius, innerCirclePaint);
        gameRockerViewBg.draw(canvas, pictureChange);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                handleEvent(event);
                break;
            case MotionEvent.ACTION_MOVE:
                handleEvent(event);
                break;
            case MotionEvent.ACTION_UP:
                restorePosition();
                break;
        }

        return true;
    }

    /**
     * 处理手势事件
     */
    private void handleEvent(MotionEvent event) {
        float gx = event.getX();
        float gy = event.getY();
        float x = gx;
        float y = gy;
        if (x > size) x = size;
        if (y > size) y = size;
        if (x > outerCircleRadius) x = x - outerCircleRadius;
        else x = outerCircleRadius - x;
        if (y > outerCircleRadius) y = y - outerCircleRadius;
        else y = outerCircleRadius - y;

        double distance = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)); // 触摸点与view中心距离
        x = gx;
//        double angle = Math.toDegrees(Math.acos((x*x+distance*distance-y*y)/(2*x*distance)));
        // 四个象限12个方向判断 方向一直不方信号
        if (distance < outerCircleRadius-innerCircleRadius) {
            //在自由域之内，触摸点实时作为内圆圆心
            innerCenterX = innerCircleBoundaryX(gx, gx);
            innerCenterY = gy;
        } else {
            //在自由域之外，内圆圆心在触摸点与外圆圆心的线段上
            updateInnerCircelCenter(event);
        }
        invalidate();
        if (lastCommand != 1 && gy < outerCircleRadius) {
            pictureChange = 1;
            sendCarCommand("CarControl", 1, "going");
            System.out.println("前进");
        } else if (lastCommand != 2 && gy > outerCircleRadius) {
            pictureChange = 2;
            System.out.println("后退");
            sendCarCommand("CarControl", 2, "backing");
        }
    }

    /**
     * 在自由域外更新内圆中心坐标
     */
    private void updateInnerCircelCenter(MotionEvent event) {
        double distance = Math.sqrt(Math.pow(event.getX()-viewCenterX, 2) + Math.pow(event.getY()-viewCenterY, 2));  //当前触摸点到圆心的距离
        int innerDistance = outerCircleRadius-innerCircleRadius;  //内圆圆心到中心点距离
        //相似三角形的性质，两个相似三角形各边比例相等得到等式
        innerCenterX = (event.getX()-viewCenterX)*innerDistance/distance + viewCenterX;
        innerCenterX = innerCircleBoundaryX(event.getX(), innerCenterX);
        innerCenterY = (event.getY()-viewCenterY)*innerDistance/distance + viewCenterY;
        invalidate();
    }

    private double innerCircleBoundaryX(float x, double value) {
        if (middleCircleRadius-innerCircleRadius+outerCircleRadius < x) {
            return middleCircleRadius-innerCircleRadius+outerCircleRadius;
        }
        if (outerCircleRadius+innerCircleRadius-middleCircleRadius> x) {
            return outerCircleRadius+innerCircleRadius-middleCircleRadius;
        }
        return value;
    }

    /**
     * 恢复内圆到view中心位置
     */
    private void restorePosition() {
        innerCenterX = viewCenterX;
        innerCenterY = viewCenterY;
        pictureChange = 0;
        sendCarCommand("CarControl", 0, "stop");
        invalidate();
    }

    public void setParameter(GizWifiDevice device, GosCarControlActivity gosCarControlActivity) {
        this.device = device;
        this.gosCarControlActivity = gosCarControlActivity;
    }

    private void sendCarCommand(String type, int currentCommand, String value) {
        if (lastCommand != currentCommand) {
            int code = GizWifiSDK.sharedInstance().post(device.m_index,type,value);
            lastCommand = currentCommand;
        }
    }
}
