package com.gizwits.opensource.appkit.view;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.gizwits.opensource.appkit.ControlModule.GosCarControlActivity;
import com.gizwits.opensource.appkit.R;
import com.hisilicion.histreaming.GizWifiDevice;
import com.hisilicion.histreaming.GizWifiSDK;

public class SpeedRemoteView extends View {
    private static int angleRect = 30;
    private static int top = 50;
    private Paint outerRectangle;
    private Paint innerRectangle;
    private float innerTop = 270;

    private GizWifiDevice device;

    private GosCarControlActivity gosCarControlActivity;

    double lastSpeed = 10; // 默认值

    // 初始状态50速度
    private RectF outerRectF = new RectF(0, 0,70,300);
    private RectF innerRectF = new RectF(0, innerTop,70,300);

    public SpeedRemoteView(Context context) {
        super(context);
        init();
    }

    public SpeedRemoteView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init() {
        outerRectangle = new Paint();
        outerRectangle.setColor(getResources().getColor(R.color.grey));
        outerRectangle.setAntiAlias(true);
        outerRectangle.setMaskFilter(new BlurMaskFilter(50, BlurMaskFilter.Blur.INNER));

        innerRectangle = new Paint();
        innerRectangle.setColor(getResources().getColor(R.color.white));
        innerRectangle.setAntiAlias(true);
        innerRectangle.setAlpha(130);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(70, 300);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRoundRect(outerRectF, angleRect, angleRect, outerRectangle);
        canvas.drawRoundRect(innerRectF, angleRect, angleRect, innerRectangle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                handleEvent(event);
                break;
            case MotionEvent.ACTION_MOVE:
                handleEvent(event);
                break;
        }

        return true;
    }

    private void handleEvent(MotionEvent event) {
        float y = event.getY();
        if (y <= 0) {
            innerTop = 0;
        } else if (y >= 300) {
            innerTop = 300;
        } else {
            innerTop = y;
        }
        double speed = Math.floor((300-innerTop)/3);
        innerRectF.top = innerTop;
        if (speed == 100) speed--;
        gosCarControlActivity.setAssemblyValue(speed);
        if(lastSpeed != speed) {
            int code = GizWifiSDK.sharedInstance().post(device.m_index,"speedControl",String.valueOf(speed));
            lastSpeed = speed;
        }
        invalidate();

    }

    public void setParameter(GizWifiDevice device, GosCarControlActivity gosCarControlActivity) {
        this.device = device;
        this.gosCarControlActivity = gosCarControlActivity;
        GizWifiSDK.sharedInstance().post(device.m_index,"speedControl",String.valueOf(10));
    }
}
