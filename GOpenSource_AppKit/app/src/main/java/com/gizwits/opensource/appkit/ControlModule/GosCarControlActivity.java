package com.gizwits.opensource.appkit.ControlModule;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gizwits.opensource.appkit.CommonModule.GosBaseActivity;
import com.gizwits.opensource.appkit.R;
import com.gizwits.opensource.appkit.view.GameRockerLeftView;
import com.gizwits.opensource.appkit.view.GameRockerRightView;
import com.gizwits.opensource.appkit.view.SpeedRemoteView;
import com.hisilicion.histreaming.GizWifiDevice;

public class GosCarControlActivity extends GosBaseActivity {
    private Button m_goForwardButton;
    private Button m_turnLeftButton;
    private Button m_StopButton;
    private Button m_accelerateButton;
    private Button m_decelerateButton;
    private Button m_turnRightButton;
    private Button m_turnBackButton;

    private Button m_traceButton;
    private Button m_ultrasonicButton;
    private Button m_balanceButton;

    private Button m_engine_leftButton;
    private Button m_engine_rightButton;
    private Button m_engine_middleButton;

    private TextView speedShow;

    private GameRockerLeftView gameRockerLeftView;
    private GameRockerRightView gameRockerRightView;

    private SpeedRemoteView speedRemoteView;

    private GizWifiDevice device;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_gos_car_control);
        initDevice();
        setActionBar(true,true,"CarControl");
        initView();

    }

    private void initView() {
//        m_goForwardButton = (Button)findViewById(R.id.go_forward_bt);
//        m_turnLeftButton = (Butto
//        n)findViewById(R.id.turn_left_bt);
//        m_StopButton = (Button)findViewById(R.id.stop_bt);
//        m_accelerateButton = (Button)findViewById(R.id.accelerate);
//        m_decelerateButton = (Button)findViewById(R.id.decelerate);
        speedShow = (TextView)findViewById(R.id.speedShow);
        gameRockerLeftView = (GameRockerLeftView)findViewById(R.id.remoteSensingLeft);
        gameRockerLeftView.setParameter(device, this);
        gameRockerRightView = (GameRockerRightView)findViewById(R.id.remoteSensingRight);
        gameRockerRightView.setParameter(device, this);
        speedRemoteView = (SpeedRemoteView)findViewById(R.id.SpeedRemote);
        speedRemoteView.setParameter(device, this);

//        m_turnRightButton = (Button)findViewById(R.id.turn_right_bt);
//        m_turnBackButton = (Button)findViewById(R.id.turn_back_bt);
//        m_traceButton = (Button)findViewById(R.id.trace_bt);
//        m_ultrasonicButton = (Button)findViewById(R.id.ultrasonic_bt);
//        m_balanceButton = (Button)findViewById(R.id.balance_bt);

//        m_engine_leftButton = (Button)findViewById(R.id.engine_left_bt);
//        m_engine_rightButton = (Button)findViewById(R.id.engine_right_bt);
//        m_engine_middleButton = (Button)findViewById(R.id.engine_middle_bt);

//        m_goForwardButton.setOnClickListener(new ButtonListener());
//        m_turnLeftButton.setOnClickListener(new ButtonListener());
//        m_StopButton.setOnClickListener(new ButtonListener());
//        m_turnRightButton.setOnClickListener(new ButtonListener());
//        m_turnBackButton.setOnClickListener(new ButtonListener());
//        m_traceButton.setOnClickListener(new ButtonListener());
//        m_ultrasonicButton.setOnClickListener(new ButtonListener());
//        m_balanceButton.setOnClickListener(new ButtonListener());
//
//        m_goForwardButton.setOnClickListener(new ButtonListener());
//        m_turnBackButton.setOnClickListener(new ButtonListener());
//        m_turnLeftButton.setOnClickListener(new ButtonListener());
//        m_turnRightButton.setOnClickListener(new ButtonListener());
//
//        m_engine_leftButton.setOnClickListener(new ButtonListener());
//        m_engine_rightButton.setOnClickListener(new ButtonListener());
//        m_engine_middleButton.setOnClickListener(new ButtonListener());
    }


    private void initDevice() {
        Intent intent = getIntent();
        device = (GizWifiDevice) intent.getParcelableExtra("GizWifiDevice");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class ButtonListener implements View.OnClickListener {

        @SuppressLint("WrongConstant")
        @Override
        public void onClick(View view) {
            int code = -2;
            switch (view.getId()){
//                case R.id.ultrasonic_bt:
//                    code = GizWifiSDK.sharedInstance().post(device.m_index,"ModularControl","ultrasonic");
//                    if(code == 0){
//                        Toast.makeText(GosCarControlActivity.this, "ultrasonic", 100).show();//add by wgm
//                    }else if(code == -2){
//                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
//                    }
//                    break;
//                case R.id.trace_bt:
//                    code = GizWifiSDK.sharedInstance().post(device.m_index,"ModularControl","trace");
//                    if(code == 0){
//                        Toast.makeText(GosCarControlActivity.this, "trace", 100).show();//add by wgm
//                    }else if(code == -2){
//                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
//                    }
//                    break;
//                case R.id.go_forward_bt: //前进
//                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","going");
//                    if(code == 0){
//                        Toast.makeText(GosCarControlActivity.this, "going", 100).show();//add by wgm
//                    }else if(code == -2){
//                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
//                    }
//                    break;
//                case R.id.turn_left_bt:  //向左
//                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","lefting");
//                    if(code == 0){
//                        Toast.makeText(GosCarControlActivity.this, "lefting", 100).show();//add by wgm
//                    }else if(code == -2){
//                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
//                    }
//                    break;
//                case R.id.stop_bt:       //暂停
//                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","stop");
//                    if(code == 0){
//                        Toast.makeText(GosCarControlActivity.this, "stop", 100).show();//add by wgm
//                    }else if(code == -2){
//                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
//                    }
//                    break;
//                case R.id.accelerate:       //加速
//                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","accelerate");
//                    if(code == 0){
//                        Toast.makeText(GosCarControlActivity.this, "accelerate", 100).show();//add by wgm
//                    }else if(code == -2){
//                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
//                    }
//                    break;
//                case R.id.decelerate:       //减速
//                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","decelerate");
//                    if(code == 0){
//                        Toast.makeText(GosCarControlActivity.this, "decelerate", 100).show();//add by wgm
//                    }else if(code == -2){
//                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
//                    }
//                    break;
//                case R.id.turn_right_bt: //向右
//                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","righting");
//                    if(code == 0){
//                        Toast.makeText(GosCarControlActivity.this, "righting", 100).show();//add by wgm
//                    }else if(code == -2){
//                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
//                    }
//                    break;
//                case R.id.turn_back_bt:  //向后
//                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","backing");
//                    if(code == 0){
//                        Toast.makeText(GosCarControlActivity.this, "backing", 100).show();//add by wgm
//                    }else if(code == -2){
//                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
//                    }
//                    break;
//
//                case R.id.engine_left_bt:  //舵机左
//                    code = GizWifiSDK.sharedInstance().post(device.m_index,"ModularControl","engine_right");
//                    if(code == 0){
//                        Toast.makeText(GosCarControlActivity.this, "engine_right", 100).show();//add by wgm
//                    }else if(code == -2){
//                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
//                    }
//                    break;
//                case R.id.engine_right_bt:  //舵机右
//                    code = GizWifiSDK.sharedInstance().post(device.m_index,"ModularControl","engine_left");
//                    if(code == 0){
//                        Toast.makeText(GosCarControlActivity.this, "engine_left", 100).show();//add by wgm
//                    }else if(code == -2){
//                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
//                    }
//                    break;
//                case R.id.engine_middle_bt:  //舵机中
//                    code = GizWifiSDK.sharedInstance().post(device.m_index,"ModularControl","engine_middle");
//                    if(code == 0){
//                        Toast.makeText(GosCarControlActivity.this, "engine_middle", 100).show();//add by wgm
//                    }else if(code == -2){
//                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
//                    }
//                    break;
//                case R.id.balance_bt:  //舵机中
//                    code = GizWifiSDK.sharedInstance().post(device.m_index,"ModularControl","balance");
//                    if(code == 0){
//                        Toast.makeText(GosCarControlActivity.this, "balance", 100).show();//add by wgm
//                    }else if(code == -2){
//                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
//                    }
//                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 供自定义view组件修改界面值使用
     * @param speedTextView 修改显示速度的TextView组件值
     */
    public void setAssemblyValue(double speedTextView) {
        speedShow.setText("当前速度:"+Integer.valueOf((int)speedTextView));
    }
}
