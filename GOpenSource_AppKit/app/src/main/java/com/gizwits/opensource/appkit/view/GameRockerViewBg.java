package com.gizwits.opensource.appkit.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class GameRockerViewBg {
    private Bitmap bitmapBg0;
    private Bitmap bitmapClickBg0;
    private Bitmap bitmapBg1;
    private Bitmap bitmapClickBg1;

    private Rect src0;
    private Rect srcClick0;
    private Rect src1;
    private Rect srcClick1;

    private Rect dst0;
    private Rect dst1;

    private Paint paint = new Paint();

    public GameRockerViewBg(Bitmap bitmap0, Bitmap bitmapClickBg0, Bitmap bitmap1, Bitmap bitmapClickBg1, Rect dst0, Rect dst1) {
        this.bitmapBg0 = bitmap0;
        this.bitmapClickBg0 = bitmapClickBg0;
        this.bitmapBg1 = bitmap1;
        this.bitmapClickBg1 = bitmapClickBg1;
        this.dst0 = dst0;
        this.dst1 = dst1;
        this.src0 = new Rect(0, 0, bitmap0.getWidth(), bitmap0.getHeight());
        this.srcClick0 = new Rect(0, 0, bitmapClickBg0.getWidth(), bitmapClickBg0.getHeight());
        this.src1 = new Rect(0, 0, bitmap1.getWidth(), bitmap1.getHeight());
        this.srcClick1 = new Rect(0, 0, bitmapClickBg1.getWidth(), bitmapClickBg1.getHeight());
    }

    //背景的绘图函数
    public void draw(Canvas canvas, int choice) {
        if (choice == 0) {
            canvas.drawBitmap(bitmapBg0, src0, dst0, paint);
            canvas.drawBitmap(bitmapBg1, src1, dst1, paint);
        } else if (choice == 1) {
            canvas.drawBitmap(bitmapClickBg0, srcClick0, dst0, paint);
            canvas.drawBitmap(bitmapBg1, src1, dst1, paint);
        } else if (choice == 2) {
            canvas.drawBitmap(bitmapBg0, src0, dst0, paint);
            canvas.drawBitmap(bitmapClickBg1, srcClick1, dst1, paint);
        }
    }
}
